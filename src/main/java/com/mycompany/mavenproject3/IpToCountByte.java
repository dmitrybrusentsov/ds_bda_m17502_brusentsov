package com.mycompany.mavenproject3;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;

/**
 * Программный класс который подсчитывает среднее количество байт на запрос по
 * IP и общее количество байт по IP. Входными данными служат строки текстового
 * файла. На выходе SequenceFile файл со строками: IP, 175.5, 109854.
 *
 * @version 1.0
 * @author Дмитрий Брусенцов
 */
public class IpToCountByte {

    /**
     * Класс карт - Mapper. Предназначен для разбора строки входного файла на ip
     * - новый ключ (key), количества байт - новое значение (value) и записи
     * полученного результата в Context
     */
    public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {

        /**
         * Метод map
         *
         * @param key - ключ
         * @param value - значение
         * @param context - контекст
         * @throws IOException
         * @throws InterruptedException
         */
        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {
            //Получение строки из входного файла
            String line = value.toString();
            //Парсим строку по пробелам в массив token
            String[] token = line.split(" ");
            //Если массив не создался, если он пустой, 
            //если он меньше размера заданного шаблона
            if (token == null || token.length == 0 || token.length < 9) {
                //Увеличиваем значения (+1) счетчика "плохих" строк
                context.getCounter("COUNT", "BUGS_ROWS").increment(1);
            } else {
                //Если оба token не пустые то пишем в context ключ и значение
                if (!token[0].isEmpty() && !token[8].isEmpty()) {
                    //В token[0] находится ip
                    value.set(token[0]);
                    //Записываем в key = ip, value = count_byte
                    //В token[8] находится количество байт
                    context.write(value, new IntWritable(Integer.valueOf(token[8])));
                } else {
                    //Увеличиваем значения (+1) счетчика "плохих" строк
                    context.getCounter("COUNT", "ROWS").increment(1);
                }
            }
        }
    }

    /**
     * Класс карт - Reducer. Предназначен для подсчета по каждому ip количества
     * байт и среднего значения байт. ip - новый ключ (key), среднее значение
     * количества байт, общее количество байт - новое значение (value) и записи
     * полученного результата в Context
     */
    public static class Reduce extends Reducer<Text, IntWritable, Text, Text> {

        /**
         * Метод reduce
         *
         * @param key - ключ
         * @param values - значение
         * @param context - контекст
         * @throws IOException
         * @throws InterruptedException
         */
        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            //Счетчик суммы всех байт по конкретному ip
            int sum = 0;
            //Счетчик по поличеству строк с одинаковым ip
            int k = 0;
            for (IntWritable x : values) {
                //Суммируем по конкретному ip все байты
                sum += x.get();
                k++;
            }
            //Строка среднее значение количества байт, общее количество байт
            String str = String.valueOf(new Double(sum / k)) + ", " + String.valueOf(sum);
            context.write(key, new Text(str));
        }
    }

    /**
     * Метод для запуска задания в Hadoop
     *
     * @param args - имена 2 файлов, 1 имя - файл с входнымм данными 2 имя -
     * файл куда будет записан результат
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //Инициализация задания Hadoop
        Configuration conf = new Configuration();
        Job job = new Job(conf, "My IP Count BYTE Program");
        //Устанавливаем главный файл запуска задания
        job.setJarByClass(IpToCountByte.class);
        //Установить MapClass и ReduceClass в работу
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
        //Установить Ключи и Значения для выхода Mapper
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        //Установить Ключи и Занчения для выхода
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        //Настройка файловых форматов для входа и выхода
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        Path outputPath = new Path(args[1]);
        //Настройка пути ввода / вывода из файловой системы в задание
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if (job.isSuccessful()) {
            System.out.println("TASK SUCCESS");
            long bugsRows = job.getCounters().findCounter("COUNT", "BUGS_ROWS").getValue();
            System.out.println("COUNT_BUGS_ROWS = " + bugsRows); 
        } else if (!job.isSuccessful()) {
            System.out.println("TASK ERROR");
        }
        //Выход из задания, только если значение флага равно false
        System.exit(returnValue);
    }
}
