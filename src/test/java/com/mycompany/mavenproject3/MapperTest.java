package com.mycompany.mavenproject3;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;

/**
 * Тестовый класс для проверки Mapper
 *
 * @version 1.0
 * @author Дмитрий Брусенцов
 */
public class MapperTest {

    /**
     * Драйвер для Mapper
     */
    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

    /**
     * Установка драйвера для Mapper
     */
    @Before
    public void setUp() {
        //Создание объекта Mapper
        IpToCountByte.Map mapper = new IpToCountByte.Map();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    /**
     * Тестовый метод
     *
     * @throws IOException
     */
    @Test
    public void testMapper() throws IOException {
        //Входные данные
        mapDriver.withInput(new LongWritable(100), new Text("62.2.6.4 - - [16/Oct/2018:1:4:37 -0400]POST ../tmp/photo9789/1130-5266.jpg HTTP/1.1 379 20 -Google Chrome/70.0 (Images/3.0; +http://bdvjcbjpxi.com)"));
        //Данные на выходе
        mapDriver.withOutput(new Text("62.2.6.4"), new IntWritable(20));
        //Запуск теста
        mapDriver.runTest();
    }
}
