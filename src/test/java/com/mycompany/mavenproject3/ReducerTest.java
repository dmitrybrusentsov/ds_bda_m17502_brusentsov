package com.mycompany.mavenproject3;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;

/**
 * Тестовый класс для проверки Reducer
 *
 * @version 1.0
 * @author Дмитрий Брусенцов
 */
public class ReducerTest {

    /**
     * Драйвер для Reducer
     */
    ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;

    /**
     * Установка драйвера для Reducer
     */
    @Before
    public void setUp() {
        //Создание объекта Reducer
        IpToCountByte.Reduce reducer = new IpToCountByte.Reduce();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    /**
     * Тестовый метод
     *
     * @throws IOException
     */
    @Test
    public void testReducer() throws IOException {
        //Буфер для хранения количества байт
        ArrayList<IntWritable> values = new ArrayList<>();
        values.add(new IntWritable(30));
        values.add(new IntWritable(50));
        //Входные данные
        reduceDriver.withInput(new Text("62.2.6.4"), values);
        //Счетчик суммы всех байт по ip
        int sum = 0;
        //Счетчик по поличеству строк с одинаковым ip
        int k = 0;
        for (IntWritable x : values) {
            //Суммируем по конкретному ip все байты
            sum += x.get();
            k++;
        }
        //Строка со средним значением количества байт, и общим количеством байт
        String str = String.valueOf(new Double(sum / k)) + ", " + String.valueOf(sum);
        //Данные на выходе
        reduceDriver.withOutput(new Text("62.2.6.4"), new Text(str));
        //Запуск теста
        reduceDriver.runTest();
    }
}
